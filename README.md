# Evolve-X
An app meant to host images, videos, and possibly shorten links.

Looking for path documentation? Head to [the paths documentation here](./Paths.md)

or look through the html version [here](https://evolvex.docs.apiary.io/#)

Looking for the config perhaps? Check out [The config documentation](./Config.md)

# Requirements

- Node
- Typescript
- Git
- Yarn, for the update command.
- MongoDB

# Quick start

- Linux users could benefit from NGINX.

View [The install wiki](https://gitlab.com/evolve-x/evolve-x/-/wikis/install)

# Made & Maintained by

Developer: Null (VoidNulll)

Designer: Catbirby

Contributors.

# Licensing and code

[LICENSE](https://www.gnu.org/licenses/agpl-3.0.html)

I try to make it known where I get my code from.

Any code without an author stated somewhere within/around the function/line of code is assumed to be my own.

If you have any complaints on code stealing (and this is from the original [Evolve-X Repository](https://gitlab.com/evolve-x/evolve-x)), please

- Check the development branch version. That is usually up to date.
- If not found in the development branch create an issue on the gitlab with the tag "Copyright Issue" and the line of code. I will see if this code was taken from anywhere.

# Copyright

Copyright (C) 2019 VoidNulll
