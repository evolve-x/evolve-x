/**
 * @license
 *
 * Evolve-X is an open source image host. https://gitlab.com/evolve-x
 * Copyright (C) 2019 VoidNulll
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.

 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import Path from '../../Structures/Path';
import Evolve from '../../Structures/Evolve';
import Base from '../../Structures/Base';
import { Response } from 'express';

class Signup extends Path {
    constructor(evolve: Evolve, base: Base) {
        super(evolve, base);
        this.label = '[API] Signup';

        this.path = '/api/signup';
        this.type = 'post';
    }

    async genUID(): Promise<string> {
        // Generate an ID, and do not allow a users id to be reused
        const uID = await this.Utils.genUID();
        const user = await this.base.schemas.User.findOne( { uID } );
        if (user) { // If the user was found, retry
            return this.genUID();
        }
        // Return user id
        return uID;
    }

    async execute(req: any, res: any): Promise<Response> {
        // If signups are closed, state that and do not allow them through
        if (!this.base.options.signups) {
            return res.status(this.codes.locked).send('[ERROR] Signup\'s are closed.');
        }

        // Check all required body is there
        if (!req.body || (req.body && (!req.body.username || !req.body.password) ) ) {
            if (req.body && (req.body.username || req.body.password) ) {
                return res.status(this.codes.badReq).send('[ERROR] MISSING DETAIL');
            }
            return res.status(this.codes.badReq).send('[ERROR] MISSING ALL DETAILS');
        }

        // Fetch the username and password from the body
        const { username, password } = req.body;
        // Max and min username lengths
        const maxUsername = 12;
        const minUsername = 3;
        // If the username length does not match criteria
        if (username.length > maxUsername || username.length < minUsername) {
            return res.status(this.codes.badReq).send('[ERROR] Username must be between 3 and 12 characters!');
        } if (username.length !== username.match(/[a-z0-9_]/g).length) { // If the username doess not match our username pattern
            return res.status(this.codes.badReq).send('[ERROR] Username may only contain lowercase letters, numbers, and an underscore.');
        }

        // See if the username is already taken. If its taken error the request with a code of "IM USED"
        const user = await this.base.schemas.User.findOne( { username } ) || await this.base.schemas.VerifyingUser.findOne( { username } );
        if (user) {
            return res.status(this.codes.used).send('[ERROR] Username taken!');
        }

        // Minimum and max password lengths
        const minPass = 8;
        const maxPass = 32;
        // If the password is not over min length
        // If password does not match the regex completely
        const match: RegExpMatchArray | null = password.match(/[A-Za-z0-9_.&]/g);
        if (password.length < minPass || (match && match.length !== password.length) ) {
            return res.status(this.codes.badReq).send('Password must be 8 characters or more long, and be only contain alphanumeric characters as well as `.`, and `&`');
        }
        // If the password is too long
        if (password.length > maxPass) {
            return res.status(this.codes.badReq).send('Password is too long, password must be under 32 characters long');
        }

        // Hash the password and catch errors
        let pswd;
        try {
            pswd = await this.Utils.hashPass(password);
        } catch (err) {
            // Errors shouldnt happen here, so notify the console.. Also notify the user
            console.log(`[ERROR] [SIGNUP -  Create password] - ${err}`);
            return res.status(this.codes.internalErr).send(`[ERROR] ${err.message}`);
        }

        // Generate the user ID and validation token.
        const uID = await this.genUID();
        const validationToken = await this.Utils.genValidationToken();
        // Add the user to the VerifyingUser database and save
        const nUser = new this.base.schemas.VerifyingUser( { uID, password: pswd, username, validationToken: validationToken.hash } );
        await nUser.save();

        // Find admin notifications, and generate an ID
        const notifs = await this.base.schemas.AdminNotifs.find();
        const notifyID = await this.Utils.genNotifyID(notifs);
        // Make a new notification and save to database
        const notify = new this.base.schemas.AdminNotifs( {
            ID: notifyID,
            title: 'New user signup!',
            notify: `Username: ${username}\nUser ID: ${uID}\nValidation Token: ${validationToken.token}`,
        } );
        await notify.save();
        // Notify the console, and the user that the admins have been notified.
        console.log(`[SYSTEM - SIGNUP] Notified admins about verifying user ${uID}`);
        this.base.Logger.log('SYSTEM - SIGNUP', `New user signed up to Evolve-X`, { user: `${username} (${uID})` }, 'signup', 'New user signup');
        return res.status(this.codes.created).send('[SUCCESS] The admins have been notified of your account request!');
    }
}

export default Signup;
